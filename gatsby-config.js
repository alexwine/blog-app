const siteMetadata = {
  title: `Alex Site`,
  name: `Alex Rules`,
  siteUrl: `https://novela.narative.co`,
  description: `This is my description that will be used in the meta tags and important for search results`,
  hero: {
    heading: `Random projects and musings.`,
    maxWidth: 652,
  },
  social: [
    // {
    //   name: `twitter`,
    //   url: `https://twitter.com/narative`,
    // },
    {
      name: `github`,
      url: `https://github.com/alexwine36`,
    },
    // {
    //   name: `instagram`,
    //   url: `https://instagram.com/narative.co`,
    // },
    {
      name: `linkedin`,
      url: `https://www.linkedin.com/in/alexwine/`,
    },
    // {
    //   name: `dribbble`,
    //   url: `https://dribbble.com/narativestudio`,
    // },
  ],
};

const plugins = [
  {
    resolve: "@narative/gatsby-theme-novela",
    options: {
      contentPosts: "content/posts",
      contentAuthors: "content/authors",
      basePath: "/",
      authorsPage: true,
      sources: {
        local: true,
        // contentful: true,
      },
    },
  },
  {
    resolve: `gatsby-plugin-manifest`,
    options: {
      name: `Novela by Narative`,
      short_name: `Novela`,
      start_url: `/`,
      background_color: `#fff`,
      theme_color: `#fff`,
      display: `standalone`,
      icon: `src/assets/favicon.png`,
    },
  },
  {
    resolve: `gatsby-plugin-netlify-cms`,
    options: {},
  },
  "gatsby-plugin-typescript",
  // {
  //   resolve: `gatsby-plugin-google-analytics`,
  //   options: {
  //     trackingId: "G-ZZ1F6PVT0X",
  //   },
  // },
  {
    resolve: "gatsby-plugin-google-gtag",
    options: {
      trackingIds: ["G-ZZ1F6PVT0X"],
    },
  },
];

/**
 * For development purposes if there's no Contentful Space ID and Access Token
 * set we don't want to add in gatsby-source-contentful because it will throw
 * an error.
 *
 * To enanble Contentful you must
 * 1. Create a new Space on contentful.com
 * 2. Import the Contentful Model from @narative/gatsby-theme-novela/conteful
 * 3. Add .env to www/ (see www/env.example)
 * 4. Enable contentful as a source in this file for @narative/gatsby-theme-novela
 */
if (process.env.CONTENTFUL_SPACE_ID && process.env.CONTENTFUL_ACCESS_TOKEN) {
  plugins.push({
    resolve: "gatsby-source-contentful",
    options: {
      spaceId: process.env.CONTENTFUL_SPACE_ID,
      accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
    },
  });
}

module.exports = {
  siteMetadata,
  plugins,
};
